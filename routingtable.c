/* ECE 463 Project 2 submission.
 * This file implements functions that manipulate the routing table. 
 * Joe Mynhier
 */
#include <stdio.h>
#include "ne.h"
#include "router.h"

// Global variable that contains all route entries.
struct route_entry routingTable[MAX_ROUTERS] = {0};

// Global variable that tracks the number of entries in the routing table.
int NumRoutes;

// Helper that add an entry to the routing table.
void addRoute(unsigned int dest_id, unsigned int next_hop, unsigned int cost) {
    if (dest_id < MAX_ROUTERS) {
        routingTable[dest_id].dest_id = dest_id;
        routingTable[dest_id].next_hop = next_hop;
        routingTable[dest_id].cost = cost;
        NumRoutes += 1;
    }
}

// Starts a new router table.
void InitRoutingTbl (struct pkt_INIT_RESPONSE *InitResponse, int myID) {
    int i;
    for (i=0; i<MAX_ROUTERS; i++) {
        routingTable[i].dest_id = INFINITY;
    }

    addRoute(myID, myID, 0);
    
    for (i=0; i<InitResponse->no_nbr; i++) {
        addRoute(InitResponse->nbrcost[i].nbr,
                 InitResponse->nbrcost[i].nbr,
                 InitResponse->nbrcost[i].cost);
    }

    routingTable[myID].dest_id = myID;
    routingTable[myID].next_hop = myID;
    routingTable[myID].cost = 0;
}

// Updates the routing table.
int UpdateRoutes(struct pkt_RT_UPDATE *RecvdUpdatePacket, int costToNbr, int myID) {
    int return_val = 0;
    int count = 0;
    if (myID == RecvdUpdatePacket->dest_id) {
        unsigned int sender = RecvdUpdatePacket->sender_id;
        int i;
        for (i=0; i<RecvdUpdatePacket->no_routes; i++) {
            unsigned int destination = RecvdUpdatePacket->route[i].dest_id;
            unsigned int senders_next_hop = RecvdUpdatePacket->route[i].next_hop;
            unsigned int senders_cost = RecvdUpdatePacket->route[i].cost;
    
            if (destination == myID) continue;

            // Update if sender is your next hop for the destination
            // (forced update), or (the cost to destination is less
            // than our current cost and I am not the senders next hop
            // to the destination (split horizon)).
            char sender_is_next_hop = routingTable[destination].next_hop == sender;
            char sender_cost_is_less = ((senders_cost + costToNbr) < routingTable[destination].cost) || (routingTable[destination].cost == 0);
            char i_am_next_hop = myID == senders_next_hop;
            char data_is_same = routingTable[destination].dest_id == destination \
                    && (routingTable[destination].cost == costToNbr + senders_cost \
                    || (routingTable[destination].cost == INFINITY \
                    && senders_cost == INFINITY));

            if (senders_cost == INFINITY && destination != myID \
                    && !data_is_same && (!i_am_next_hop || sender_is_next_hop)) {
                
                UninstallRoutesOnNbrDeath(destination);
                return_val++;
            } else if (routingTable[destination].cost == INFINITY && senders_cost == INFINITY) {
                count += 1;
            } else if ((sender_is_next_hop || (sender_cost_is_less && !i_am_next_hop)) \
                    && !data_is_same) {
                // Increment your count if it is a new entry.
                if (routingTable[destination].cost == 0 && \
                        destination != myID) {
                    NumRoutes++;
                    return_val++;
                } else if (routingTable[destination].dest_id != destination || \
                           routingTable[destination].next_hop != sender || \
                           routingTable[destination].cost != costToNbr + senders_cost) {
                    return_val++;
                }

                routingTable[destination].dest_id = destination;
                routingTable[destination].next_hop = sender;
                routingTable[destination].cost = costToNbr + senders_cost;
    
                if (routingTable[destination].cost > INFINITY) {
                    routingTable[destination].cost = INFINITY;
                }
            }
        }
    }
    return return_val;
}

void ConvertTabletoPkt(struct pkt_RT_UPDATE *UpdatePacketToSend, int myID) {
    // Don't fill in the destination.
    UpdatePacketToSend->sender_id = myID;
    UpdatePacketToSend->no_routes = NumRoutes;

    // Push entries to the list.
    int count = 0;
    int my_count = 0; // If your id is 0, only send yourself once.
    int i;
    for (i=0; i<MAX_ROUTERS; i++) {
        // Add an entry to the head if it has a valid cost.
        if ((routingTable[i].cost != 0 && routingTable[i].dest_id != myID) || (routingTable[i].dest_id == myID && my_count == 0)) {
            memcpy(&(UpdatePacketToSend->route[count]), &(routingTable[i]), sizeof(struct route_entry));
            count++;

            if (routingTable[i].dest_id == myID)
                my_count = 1;
        }
    }
}

// Print to Log file.
void PrintRoutes (FILE* Logfile, int myID) {
    fprintf(Logfile, "\nRouting Table:\n");
    fflush(Logfile);
    int i;
    for (i=0; i<MAX_ROUTERS; i++) {
        if (routingTable[i].dest_id != INFINITY) {
            fprintf(Logfile, "R%d -> R%d: R%d, %d\n", myID,
                    routingTable[i].dest_id,
                    routingTable[i].next_hop,
                    routingTable[i].cost);
            fflush(Logfile);
        }
    }
}

void UninstallRoutesOnNbrDeath(int DeadNbr) {
    routingTable[DeadNbr].cost = INFINITY;
    //NumRoutes--;

    int i;
    for (i=0; i<MAX_ROUTERS; i++) {
        if (routingTable[i].next_hop == DeadNbr) {
            routingTable[i].cost = INFINITY;
        }
    }
}
