/* Submission for Lab 2 in Purude ECE463 Fall 2017. 
 * JWM
 */

#include <stdio.h>
#include <stdlib.h>         // malloc/free
#include <string.h>         // strncpy
#include <netdb.h>          // hostbyname
#include <arpa/inet.h>      // htons/l
#include <netinet/in.h>     // sockaddr_in
#include <unistd.h>         // read/write/close
#include <sys/types.h>
#include <sys/socket.h>
#include "ne.h"
#include "router.h"

int main(int argc, char **argv) {
    // Parse arguments: router_id ne_hostname ne_port router_port
    if (argc != 5) {
        printf("usage:\n\t./router router_id ne_hostname ne_port router_port\n");
        return 1; 
    }
    int router_id;
    if (sscanf(argv[1], " %i", &router_id) < 1) {
        printf("Invalid router_id\n");
        return 2;
    }
    int ne_port;
    if (sscanf(argv[3], " %i", &ne_port) < 1) {
        printf("Invalid ne_port\n");
        return 3;
    }
    int router_port;
    if (sscanf(argv[4], " %i", &router_port) < 1) {
        printf("Invalid router_port\n");
        return 3;
    }

    // Set up the UDP socket.
    int sockid = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockid <= 0) {
        return 4;
    }
    struct hostent *host; 
    if ((host = gethostbyname(argv[2])) == NULL) {
        close(sockid);
        return 5;
    }
    struct sockaddr_in sock_info = (const struct sockaddr_in){ 0 };
    sock_info.sin_family = AF_INET;
    sock_info.sin_port = htons((unsigned short) ne_port);
    strncpy((char *)&sock_info.sin_addr.s_addr,
            (char *)host->h_addr, 
            host->h_length);
    unsigned int sock_size = sizeof(sock_info);

    // Bind to my port.
    struct sockaddr_in my_sock;
    memcpy(&my_sock, &sock_info, sizeof(struct sockaddr_in));
    my_sock.sin_port = htons((unsigned short) router_port);
    bind(sockid, (const struct sockaddr *)&my_sock, sizeof(my_sock));
    
    const unsigned int buff_size = sizeof(struct pkt_RT_UPDATE) + 32;
    char *buff = malloc(buff_size);
    if (buff == NULL) {
        close(sockid);
        return 6;
    }

    // Send INIT_REQUEST
    struct pkt_INIT_REQUEST init_request;
    init_request.router_id = htonl(router_id);
    memcpy(buff, &init_request, sizeof(init_request));
    int sent = sendto(sockid, buff, sizeof(init_request), 0,
            (struct sockaddr *) &sock_info, sock_size);

    printf("sent %d sockid %d buff_size %d\n", sent, sockid, buff_size);

    // Set up table.
    if (recvfrom(sockid, buff, buff_size, 0,
            (struct sockaddr *) &sock_info, (socklen_t *)&sock_size) \
            < sizeof(struct pkt_INIT_RESPONSE)) {
        printf("Invalid init response recieved\n");
        free(buff);
        close(sockid);
        return 7;
    }
    struct pkt_INIT_RESPONSE init_response; 
    memcpy(&init_response, buff, sizeof(init_response));
    ntoh_pkt_INIT_RESPONSE(&init_response);

    unsigned int neighbour_cost[MAX_ROUTERS] = {0};
    int i; 
    for (i=0; i<init_response.no_nbr; i++) {
        neighbour_cost[init_response.nbrcost[i].nbr] = init_response.nbrcost[i].cost;
    }

    InitRoutingTbl(&init_response, router_id);

    // Open the output file.
    char filename[14];
    sprintf((char *)filename, "router%i.log", router_id);
    FILE *fout = fopen((char *)filename, "w");
    if (fout == NULL) {
        free(buff);
        close(sockid);
        return 8;
    }
    
    // Recieve other update messages.
    // Keep track of the timestamp of the last time one 
    // successfully updates your table. If it is at least
    // CONVERGE_TIMEOUT seconds, then the system has converged.
    // If an update occurs, print the new table.
    fd_set read_only_set, mutable_set;
    FD_ZERO(&read_only_set);
    FD_SET(sockid, &read_only_set);
    int set_max = sockid + 1;
    struct timeval read_only_time, mutable_time;
    read_only_time.tv_sec = UPDATE_INTERVAL;
    read_only_time.tv_usec = 0;
    mutable_time = read_only_time;

    // Track known routers. Keep track of which routers were
    // updated this cycle and a count of how many cycles it has
    // been since it was updated. If difference exedes 
    // FAILURE_DETECTION, mark as dead.
    char has_updated[MAX_ROUTERS] = {0};
    char skip_count[MAX_ROUTERS] = {0};

    int no_update_count = 0;
    int cycle_count = 0;
    int has_converged = 0;

    PrintRoutes(fout, router_id);

    while (1) {
        mutable_set = read_only_set;
        if (select(set_max, &mutable_set, NULL, NULL, &mutable_time) <= 0) {
            struct pkt_RT_UPDATE my_table;
            ConvertTabletoPkt(&my_table, router_id);

            // Check for dead connections.
            for (i=0; i<my_table.no_routes; i++) {
                int dest = my_table.route[i].dest_id;
                if (has_updated[dest]) {
                    has_updated[dest] = 0;
                    skip_count[dest] = 0;
//                    has_converged = 0;
                } else if (skip_count[dest] == FAILURE_DETECTION \
                        && dest == my_table.route[i].next_hop \
                        && dest != router_id \
                        && my_table.route[i].cost != INFINITY) {
                    // Only dead if it is a neighbor.
                    skip_count[i] = 0;
                    // Router i is dead.
                    UninstallRoutesOnNbrDeath(i);

                    //DEBUG
                    printf("%d is uninstalling %d\n", router_id, i);

                    PrintRoutes(fout, router_id);
                    has_converged = 0;
                } else {
                    skip_count[i] += UPDATE_INTERVAL;
                }
            }

            // Send out your update.
            struct pkt_RT_UPDATE packet;

            for (i=0; i<my_table.no_routes; i++) {
                if (neighbour_cost[my_table.route[i].dest_id] != 0) {
                    ConvertTabletoPkt(&packet, router_id);
                    packet.dest_id = my_table.route[i].dest_id;
                    hton_pkt_RT_UPDATE(&packet);

                    sendto(sockid, &packet, 
                           sizeof(my_table), 0,
                           (struct sockaddr *) &sock_info, 
                           sock_size);
                }
            }

            // Check for convergence.
            cycle_count += UPDATE_INTERVAL;
            if (no_update_count >= CONVERGE_TIMEOUT && !has_converged) {
                has_converged = 1;
                fprintf(fout, "%i:Converged\n",  cycle_count);
                fflush(fout); 
            } else if (has_converged && no_update_count == 0) {
                has_converged = 0;
            }

            // Reset the timeout.
            mutable_time = read_only_time;
        } else {
            // Read a message.
            unsigned int msg_size = recvfrom(sockid, buff, 
                    buff_size, 0, (struct sockaddr *) &sock_info, 
                    (socklen_t *)&sock_size);
            // Update your table.
            if (msg_size == sizeof(struct pkt_RT_UPDATE)) {
                struct pkt_RT_UPDATE update;
                memset(&update, 0, sizeof(update));
                memcpy(&update, buff, sizeof(update));

                ntoh_pkt_RT_UPDATE(&update);

                // Set the source specific update flags.
                has_updated[update.sender_id] = 1;
                    
                if (UpdateRoutes(&update, 
                        neighbour_cost[update.sender_id],
                        router_id)) {

                    //DEBUG
                    printf("%d is printing based on %d\n", router_id, update.sender_id);

                    // Print the new table.
                    PrintRoutes(fout, router_id); 

                    // Start count over.
                    no_update_count = 0;
                    has_converged = 0;
                } else {
                    no_update_count += UPDATE_INTERVAL;
                }
            }
        }
    }
    free(buff);
    close(sockid);
    return 0;
}
